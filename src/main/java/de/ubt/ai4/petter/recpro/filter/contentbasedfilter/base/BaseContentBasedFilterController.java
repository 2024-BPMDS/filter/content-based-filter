package de.ubt.ai4.petter.recpro.filter.contentbasedfilter.base;

import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/filter/contentBased/base")
@AllArgsConstructor
public class BaseContentBasedFilterController {

    private BaseContentBasedFilterService contentBasedFilterService;

    @GetMapping("/bpmElements")
    public ResponseEntity<Tasklist> getByBpmElements(@RequestParam Long tasklistId, String filterId, @RequestHeader("X-User-ID") String userId) {
        return  ResponseEntity.ok(contentBasedFilterService.executeFilter(tasklistId, filterId, userId));
    }

}
