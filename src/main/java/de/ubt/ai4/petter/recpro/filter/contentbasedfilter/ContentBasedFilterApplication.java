package de.ubt.ai4.petter.recpro.filter.contentbasedfilter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"de.ubt.ai4.petter.recpro.filter.contentbasedfilter", "de.ubt.ai4.petter.recpro.filter.lib.recproapi"})
public class ContentBasedFilterApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContentBasedFilterApplication.class, args);
	}

}
