package de.ubt.ai4.petter.recpro.filter.contentbasedfilter.base;

import de.ubt.ai4.petter.recpro.filter.lib.recproapi.util.RecproDataService;
import de.ubt.ai4.petter.recpro.lib.attribute.modeling.model.RecproAttribute;
import de.ubt.ai4.petter.recpro.lib.attributepersistence.model.RecproAttributeInstance;
import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.Activity;
import de.ubt.ai4.petter.recpro.lib.bpmpersistence.model.persistence.Tasklist;
import de.ubt.ai4.petter.recpro.lib.filter.basefilter.model.Filter;
import de.ubt.ai4.petter.recpro.lib.rating.ratingpersistence.model.RatingInstance;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@AllArgsConstructor
public class BaseContentBasedFilterService {

    private RecproDataService recproDataService;

    public Tasklist executeFilter(Long tasklistId, String filterId, String userId) {
        Tasklist tasklist = recproDataService.getTasklistById(tasklistId);
        Filter filter = recproDataService.getFilter(filterId);
        List<RatingInstance> ratingInstances = recproDataService.getByUserIds(new ArrayList<>(Arrays.asList(userId)));
        List<Activity> activities = recproDataService.getActivities();
        List<RecproAttribute> attributes = recproDataService.getAttributes();
        List<RecproAttributeInstance> attributeInstances = recproDataService.getAllAttributeInstances();
        return tasklist;
    }
}
