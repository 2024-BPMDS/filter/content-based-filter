FROM openjdk:17
# VOLUME [ "/tmp" ]
COPY target/content-based-filter.jar content-based-filter.jar
#COPY backend/src/main/resources/db/migrationScripts
ENTRYPOINT [ "java", "-jar", "/content-based-filter.jar" ]
